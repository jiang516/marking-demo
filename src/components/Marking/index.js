import Component from './src/Main.vue'

Component.install = function (Vue) {
  Vue.component(Component.name, Component)
}

export default Component
