### Marking 阅卷  

详细使用方法请参考demo

----


### 属性  


---
| 属性名      | 说明    | 类型      | 可选值       | 默认值   |
|---------- |-------- |---------- |-------------  |-------- |
| load-fn     | 数据加载方法,该方法参数为调用load是传入的参数   | Function(arg)  |   —            |    —     |
| image-load-fn     | 图片加载方法   | Function    |   — |     —    |
| select-bg-color     | 文本选区背景颜色   | String    | — | rgba(0, 180, 255, 0.48)   |
| clip-toolbar  | 截图工具条    | Array   | —   | —   |


### 事件


---
| 事件名      | 说明    | 参数      | 
|---------- |-------- |---------- |
| progress     | 进度   | { progress }  |   
| flag-click     | 标记旗帜click事件   | { markid }    |
| flag-mouseover  | 标记旗帜mouseover事件    | { markid }   | 
| flag-mouseleave  | 标记旗帜mouseleave事件    | { markid }   |
| selection-change  | 选区变化    | { selection, evt }   |
| clip-state-change  | 截图状态改变    | { state }   | 
| marking-contextmenu  | 右键菜单    | { selection, evt }   | 


### 方法


---
| 方法名      | 说明    | 参数      | 
|---------- |-------- |---------- |
| switchCursorTool     | 选择文本工具或手形工具  | 'select' / 'hand'  |   
| copySelectText     | 将选中文本复制到剪贴板   | —    |  
| toggleMarkShow     | markList显示或隐藏   | —    | 
| getSelection     | 获取选区   | —    |
| clearSelection  | 清除选区    | —   | 
| reload  | 重新加载    | —  |
| load  | 加载，只有传入的第一个参数有用，该参数于load-fn绑定的方法参数中得到体现    |    |
| open  | 打开    | { data,markList,pageIndex }   | 
| setScale  | 设置缩放比例（实际大小：original，适合页面：page,适合页宽：page-fit）    |  'original' / 'page' / 'page-fit' / Number  | 
| zoomOut  | 缩小    | —   | 
| zoomIn  | 放大    | —   | 
| clockwise  | 顺时针旋转    | —   | 
| antiClockwise  | 逆时针旋转    | —   | 
| clip  | 截图    | —   | 
| skipPage  | 根据页码索引进行页码跳转    | pageIndex   | 
