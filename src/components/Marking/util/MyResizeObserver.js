export default class MyResizeObserver {
  constructor (el, fn, immediate) {
    if (immediate) {
      fn({
        width: el.offsetWidth,
        height: el.offsetHeight
      })
    }
    let ro = new ResizeObserver(entries => {
      fn({
        width: entries[0].contentRect.width,
        height: entries[0].contentRect.height
      })
    })
    ro.observe(el)
  }
}
